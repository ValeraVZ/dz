let arr = [10, 23, '23', `stop`, 'start',null];

let dataType = prompt('Enter type: str, num, null');

if (dataType === 'str') {
  dataType = String();  
} else if (dataType === 'num') {
  dataType = Number();
} else if (dataType === 'null') {
  dataType = null;
} else { 
  dataType = undefined;
}

function filterBy (a, b) {

  let sortArr = [];
  for (let i = 0; i < arr.length; i++) {
   if (!(typeof(arr[i]) == typeof(dataType))) {
      sortArr.push(arr[i]);
  }
  }
  return  sortArr;
  } 

 let final = filterBy();

console.log(final);