const tabs = document.querySelector('.tabs');

const tabsTitle = tabs.querySelectorAll('.tabs-title');

tabs.addEventListener('click', (event) => 
{
    let target = event.target;

    tabsTitle.forEach((elem) => {
        elem.classList.remove('active');
    })

    target.classList.add('active');

    drawTabContent();
});

const tabsContentListItems = document.querySelector('.tabs-content').querySelectorAll('li');

function drawTabContent () {
    tabsTitle.forEach((elem, index) => {
        if (elem.classList.contains('active')){
            tabsContentListItems.forEach((item, position) => {
            item.classList.add('hide');

                if (position == index) {
                item.classList.remove('hide');
                }
            });
        }
    });
};

tabsTitle[0].click();